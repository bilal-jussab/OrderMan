from django.conf.urls import url
from django.contrib import admin

from .views import (
    ProductListView,
    ProductDetailView,
    ProductCreateView,
    ProductUpdateView,
)

urlpatterns = [
    url(
        r'^$',
        ProductListView.as_view(),
        name='product_list'
    ), 
    url(
        r'^(?P<pk>[0-9]+)/$',
        ProductDetailView.as_view(),
        name='product_detail'
    ),
    url(
        r'^create/$',
        ProductCreateView.as_view(),
        name='product_create'
    ),
    url(
        r'^(?P<pk>[0-9]+)/update/$',
        ProductUpdateView.as_view(),
        name='product_update'
    ),
]
