from django.conf.urls import url
from django.contrib import admin

from .views import (
    OrderListView,
    OrderDetailView,
    OrderCreateView,
    OrderUpdateView,
    OrderItemListView,
    OrderItemDetailView,
    OrderItemCreateView,
    OrderItemUpdateView,
)

urlpatterns = [
    url(
        r'^$',
        OrderListView.as_view(),
        name='order_list'
    ), 
    url(
        r'^(?P<pk>[0-9]+)/$',
        OrderDetailView.as_view(),
        name='order_detail'
    ),
    url(
        r'^create/$',
        OrderCreateView.as_view(),
        name='order_create'
    ),
    url(
        r'^(?P<pk>[0-9]+)/update/$',
        OrderUpdateView.as_view(),
        name='order_update'
    ),
    url(
        r'^items/$',
        OrderItemListView.as_view(),
        name='orderitem_list'
    ), 
    url(
        r'^items/(?P<pk>[0-9]+)/$',
        OrderItemDetailView.as_view(),
        name='orderitem_detail'
    ),
    url(
        r'^items/create/$',
        OrderItemCreateView.as_view(),
        name='orderitem_create'
    ),
    url(
        r'^items/(?P<pk>[0-9]+)/update/$',
        OrderItemUpdateView.as_view(),
        name='orderitem_update'
    ),
]
