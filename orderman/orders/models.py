# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here

class Order(models.Model):
    customer = models.ForeignKey('customers.Customer', related_name='order')
    order_date = models.DateField(blank=False, null=False)
    collection_date = models.DateField(blank=False, null=False)
    method = models.CharField(max_length=250, blank=True, null=True)
    status = models.CharField(max_length=250, blank=True, null=True)
    location = models.CharField(max_length=250, blank=True, null=True)
    ticket_number = models.PositiveIntegerField(blank=False, null=False)

    def __unicode__(self):
        return 'OR-%06d' % (self.pk)

class OrderItem(models.Model):
    product = models.ForeignKey('products.Product', related_name='order_item')
    price = models.PositiveIntegerField(blank=False, null=False)
    order = models.ForeignKey('orders.Order', related_name='order_item')
    quantity = models.PositiveIntegerField(default=1)
    order_date = models.DateField(blank=False, null=False)

    def __unicode__(self):
        return '%s, OR-%06d, %s' % (self.product, self.order.id, self.order.customer)
