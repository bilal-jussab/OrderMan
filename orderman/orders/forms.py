from django import forms

from .models import Order, OrderItem

class OrderForm(forms.ModelForm):
    
    class Meta:
        model = Order
        fields = (
            'customer',
            'collection_date',
            'order_date',
            'method',
            'status',
            'location',
            'ticket_number',
        )

class OrderItemForm(forms.ModelForm):

    class Meta:
        model = OrderItem
        fields = (
            'product',
            'price',
            'order',
            'quantity',
            'order_date',
        )
