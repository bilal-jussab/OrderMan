# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Order, OrderItem
# Register your models here.

class OrderAdmin(admin.ModelAdmin):
    list_display = ('customer', 'collection_date', 'method', 'status', 'location')


class OrderItemAdmin(admin.ModelAdmin):
    list_display = ('product', 'price', 'order', 'quantity', 'order_date')

admin.site.register(Order, OrderAdmin)
admin.site.register(OrderItem, OrderItemAdmin)
