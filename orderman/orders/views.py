# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView

from .models import Order, OrderItem
from .forms import OrderForm, OrderItemForm


class OrderListView(ListView):
    template_name = 'orders/order_list.html'
    context_object_name = 'orders'

    def get_queryset(self):
        orders = Order.objects.all()
        return orders

class OrderDetailView(DetailView):
    model = Order
    template_name = 'orders/order_detail.html'

class OrderCreateView(CreateView):
    model = Order
    form_class = OrderForm
    template_name = 'orders/order_add.html'

    def get_success_url(self):
        return reverse('orders:order_list')

class OrderUpdateView(UpdateView):
    model = Order
    form_class = OrderForm
    template_name = 'orders/order_update.html'

    def get_success_url(self):
        return reverse('orders:order_list')

class OrderItemListView(ListView):
    template_name = 'orders/order_item_list.html'
    context_object_name = 'order_items'

    def get_queryset(self):
        orderitems = OrderItem.objects.all()
        return orderitems

class OrderItemDetailView(DetailView):
    model = OrderItem
    template_name = 'orders/order_item_detail.html'

class OrderItemCreateView(CreateView):
    model = OrderItem
    form_class = OrderItemForm
    template_name = 'orders/order_item_add.html'

    def get_success_url(self):
        return reverse('orders:orderitem_list')

class OrderItemUpdateView(UpdateView):
    model = OrderItem
    form_class = OrderItemForm
    template_name = 'orders/order_item_update.html'

    def get_success_url(self):
        return reverse('orders:orderitem_list')