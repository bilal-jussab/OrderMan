# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse 
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView

from .models import Customer
from .forms import CustomerForm


class CustomerListView(ListView):
    template_name = 'customers/customer_list.html'
    context_object_name = 'customers'

    def get_queryset(self):
        customers = Customer.objects.all()
        return customers

class CustomerDetailView(DetailView):
    model = Customer
    template_name = 'customers/customer_detail.html'

class CustomerCreateView(CreateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'customers/customer_add.html'

    def get_success_url(self):
        return reverse('customers:customer_list')

class CustomerUpdateView(UpdateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'customers/customer_update.html'

    def get_success_url(self):
        return reverse('customers:customer_list')
