from django.conf.urls import url
from django.contrib import admin

from .views import (
    CustomerListView,
    CustomerDetailView,
    CustomerCreateView,
    CustomerUpdateView,
)

urlpatterns = [
    url(
        r'^$',
        CustomerListView.as_view(),
        name='customer_list'
    ),
    url(
        r'^(?P<pk>[0-9]+)/$',
        CustomerDetailView.as_view(),
        name='customer_detail'
    ),
    url(
        r'^create/$',
        CustomerCreateView.as_view(),
        name='customer_create'
    ),
    url(
        r'^(?P<pk>[0-9]+)/update/$',
        CustomerUpdateView.as_view(),
        name='customer_update'
    ),
]
